// Functions library
// Defines custom functions for the mission. Anything that needs to be called more than once should be a function
// https://community.bistudio.com/wiki/Functions_Library_(Arma_3)

// If you need to add additional functions, create a new section below using your tag of choice (Ex: SXP = Superxpdude)
// See the functions library wiki page for additional details

/*
class EX // Function TAG, used for the first part of the function name
{
	class example // Function category, defines the folder that the file is located in
	{
		class example {}; // Function class. Defines the file 
	};
};
*/

class SXP
{
	class curator
	{
		class fixCurators {};
		class fixCuratorsServer {};
		class safetyEH {postInit = 1;};
		class safetyFirst {};
	};
	class mission
	{
		class anchor {};
		class missileLaunch {};
		class respawnDisable {};
		class respawnEnable {};
		class setupBillboard {};
		class setupFlagPole {};
		class setupPackage {};
		class updateTask {};
	};
	class vehicle
	{
		class setupVehicle {};
	};
};

class MLY
{
	class mission
	{
		class handleAIDamage {};
	};
};