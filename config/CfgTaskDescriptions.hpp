// Task Descriptions
// Can be used for moving task descriptions from script files to configs


// Create new classes under here. BIS_fnc_taskCreate will use either the class passed as the description parameter (in string format)
// The following would be loaded by setting the description to "example";
// If the description is blank (""), then the task ID will be used automatically
class example
{
	title = "Example Task Title";					// Title of task. Displayed as the task name
	description = "Example Task Description";		// Description of task. Additional details displayed when the task is selected on the map screen.
	marker = "";									// Task marker. Leave blank
};

class distribution_centre
{
	title = "Find the packages";
	description = "We need to find the missing packages. They're located somewhere within the Canada Post distribution centre.";
	marker = "";
};

class locate_idap
{
	title = "Locate the first package"
	description = "The first package is located within an 'IDAP' shipping container.";
	marker = "";
};

class locate_ion
{
	title = "Locate the second package"
	description = "The second package is located within a plain black shipping container.";
	marker = "";
};

class locate_redstone
{
	title = "Locate the third package"
	description = "The third package is located within a 'Cearo' shipping container.";
	marker = "";
};

class delivery
{
	title = "Deliver the packages";
	description = "Ensure that all packages get to their correct destinations, otherwise christmas will be ruined!";
	marker = "";
};

class deliver_idap
{
	title = "Deliver the first package"
	description = "The first package is destined for the IDAP Mission HQ in Neochori.";
	marker = "";
};

class deliver_ion
{
	title = "Deliver the second package"
	description = "The second package is destined for the ION Services building in Zaros.";
	marker = "";
};

class deliver_redstone
{
	title = "Deliver the third package"
	description = "The third package is destined for the Redstone factory to the north of Lakka.";
	marker = "";
};