// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{
	class example
	{
		displayName = "Example Loadout";
		
		weapons[] = {"arifle_MXC_F", "launch_B_Titan_short_F", "hgun_ACPC2_F", "Binocular"};
		primaryWeaponItems[] = {"optic_ACO", "acc_flashlight", "30Rnd_65x39_caseless_mag"};
		secondaryWeaponItems[] = {"Titan_AP"};
		handgunItems[] = {"9Rnd_45ACP_Mag"};
		
		uniformClass = "U_B_CombatUniform_mcam_tshirt";
		headgearClass = "H_Watchcap_blk";
		facewearClass = "";
		vestClass = "V_Chestrig_khk";
		backpackClass = "B_AssaultPack_mcamo";
		
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"};
		uniformItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4}};
		vestItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4}};
		backpackItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4}};
		
		basicMedUniform[] = {};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class base
	{
		displayName = "Base Loadout";
		
		weapons[] = {"hlc_pistol_P226R_Combat", "Binocular"};
		primaryWeaponItems[] = {};
		secondaryWeaponItems[] = {};
		handgunItems[] = {"hlc_15Rnd_9x19_B_P226"};
		
		uniformClass = "TFA_CAN_des_rs";
		headgearClass = "TFAX_H_HelmetIA_can2";
		facewearClass = "";
		vestClass = "TFAx_PlateCarrierH_Tan";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap", "ItemCompass", "ItemWatch", "TFAR_anprc148jem"};
		uniformItems[] = {{"hlc_15Rnd_9x19_B_P226", 2}, {"SmokeShell", 2}, {"ItemcTabHCam", 1}};
		vestItems[] = {};
		backpackItems[] = {};
		
		basicMedUniform[] = {{"ACE_fieldDressing", 10}, {"ACE_epinephrine", 2}, {"ACE_morphine", 1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class I_Officer_F: base
	{
		displayName = "Commander";
		
		weapons[] += {"rhs_weap_m4a1_carryhandle"};
		primaryWeaponItems[] = {"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", "acc_flashlight", "optic_mrco"};
		
		headgearClass = "H_Beret_Colonel";
		facewearClass = "G_Aviator";
		backpackClass = "TFAR_anprc155_coyote";
		
		linkedItems[] += {"ItemcTab"};
		vestItems[] += {{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 10}, {"rhs_mag_m67", 2}, {"SmokeShell", 2}, {"SmokeShellGreen", 2}};
		backpackItems[] += {{"ToolKit", 1}};
	};
	
	class cmd_medic: base
	{
		displayName = "Medic";
		
		weapons[] += {"rhs_weap_m4a1_carryhandle"};
		primaryWeaponItems[] = {"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", "acc_flashlight", "optic_mrco"};
		
		headgearClass = "H_Beret_02";
		facewearClass = "G_Aviator";
		backpackClass = "TFAR_anprc155_coyote";
		
		linkedItems[] += {"ItemcTab"};
		vestItems[] += {{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 10}, {"rhs_mag_m67", 2}, {"SmokeShell", 2}, {"SmokeShellGreen", 2}};
		
		basicMedBackpack[] = {{"ACE_fieldDressing", 70}, {"ACE_epinephrine", 30}, {"ACE_morphine", 15}, {"ACE_bloodIV", 2}, {"ACE_bloodIV_500", 5}};
		
		advMedBackpack[] = {};
	};
	
	class I_Soldier_F: base
	{
		displayName = "Rifleman";
		
		weapons[] += {"rhs_weap_m16a4_carryhandle"};
		primaryWeaponItems[] = {"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", "acc_flashlight", "optic_mrco"};
		
		linkedItems[] += {"ItemMicroDAGR"};
		vestItems[] += {{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 10}, {"rhs_mag_m67", 2}, {"SmokeShell", 2}, {"SmokeShellGreen", 2}};
	};
	
	class I_Soldier_SL_F: base
	{
		displayName = "Squad Leader";
		
		weapons[] += {"rhs_weap_m16a4_carryhandle_M203"};
		primaryWeaponItems[] = {"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", "acc_flashlight", "optic_mrco", "1Rnd_HE_Grenade_shell"};
		
		backpackClass = "TFAR_anprc155_coyote";
		
		linkedItems[] += {"ItemAndroid"};
		vestItems[] += {{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 10}, {"rhs_mag_m67", 2}, {"SmokeShell", 2}, {"SmokeShellGreen", 2}};
		backpackItems[] = {{"1Rnd_HE_Grenade_shell", 15}, {"1Rnd_Smoke_Grenade_shell", 10}, {"1Rnd_SmokeGreen_Grenade_shell", 5}, {"1Rnd_SmokeRed_Grenade_shell", 10}};
	};
	
	class I_Soldier_TL_F: base
	{
		displayName = "Team Leader";
		
		weapons[] += {"rhs_weap_m16a4_carryhandle_M203"};
		primaryWeaponItems[] = {"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", "acc_flashlight", "optic_mrco", "1Rnd_HE_Grenade_shell"};
		
		backpackClass = "B_FieldPack_cbr";
		
		linkedItems[] += {"ItemMicroDAGR"};
		vestItems[] += {{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 10}, {"rhs_mag_m67", 2}, {"SmokeShell", 2}, {"SmokeShellGreen", 2}};
		backpackItems[] = {{"1Rnd_HE_Grenade_shell", 20}, {"1Rnd_Smoke_Grenade_shell", 10}, {"1Rnd_SmokeGreen_Grenade_shell", 5}, {"1Rnd_SmokeRed_Grenade_shell", 10}};
	};
	
	class I_Soldier_A_F: base
	{
		displayName = "Ammo Bearer";
		
		weapons[] += {"rhs_weap_m4a1_carryhandle"};
		primaryWeaponItems[] = {"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", "acc_flashlight", "optic_mrco"};
		
		backpackClass = "B_Kitbag_tan";
		
		linkedItems[] += {"ItemMicroDAGR"};
		vestItems[] += {{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 10}, {"rhs_mag_m67", 2}, {"SmokeShell", 2}, {"SmokeShellGreen", 2}};
		backpackItems[] = {{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 25}, {"SmokeShell", 10}};
	};
	
	class I_Soldier_AAR_F: base
	{
		displayName = "Asst. Autorifleman";
		
		weapons[] += {"rhs_weap_m4a1_carryhandle"};
		primaryWeaponItems[] = {"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", "acc_flashlight", "optic_mrco"};
		
		backpackClass = "B_Kitbag_tan";
		
		linkedItems[] += {"ItemMicroDAGR"};
		vestItems[] += {{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 10}, {"rhs_mag_m67", 2}, {"SmokeShell", 2}, {"SmokeShellGreen", 2}};
		backpackItems[] = {{"CUP_200Rnd_TE1_Red_Tracer_556x45_M249", 4}};
	};
	
	class I_Soldier_AR_F: base
	{
		displayName = "Autorifleman";
		
		weapons[] += {"CUP_lmg_minimi_railed"};
		primaryWeaponItems[] = {"CUP_200Rnd_TE1_Red_Tracer_556x45_M249", "optic_mrco"};
		
		backpackClass = "B_Kitbag_tan";
		
		linkedItems[] += {"ItemMicroDAGR"};
		vestItems[] += {{"CUP_200Rnd_TE1_Red_Tracer_556x45_M249", 1}, {"rhs_mag_m67", 2}, {"SmokeShell", 2}, {"SmokeShellGreen", 2}};
		backpackItems[] = {{"CUP_200Rnd_TE1_Red_Tracer_556x45_M249", 4}};
	};
	
	class I_medic_F: base
	{
		displayName = "Medic";
		
		weapons[] += {"rhs_weap_m4a1_carryhandle"};
		primaryWeaponItems[] = {"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", "acc_flashlight", "optic_mrco"};
		
		backpackClass = "B_Carryall_cbr";
		
		linkedItems[] += {"ItemMicroDAGR"};
		vestItems[] += {{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", 10}, {"rhs_mag_m67", 2}, {"SmokeShell", 2}, {"SmokeShellGreen", 2}};
		
		basicMedBackpack[] = {{"ACE_fieldDressing", 100}, {"ACE_epinephrine", 50}, {"ACE_morphine", 20}, {"ACE_bloodIV", 3}, {"ACE_bloodIV_500", 10}};
		
		advMedBackpack[] = {};
	};
};