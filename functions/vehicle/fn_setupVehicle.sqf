// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", nil, [objNull]],
	["_oldVeh", nil, [objNull]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {
	// Normal SUV
	case (toLower "CUP_I_SUV_ION"): {
		// Add item cargo
		[_newVeh, "suv"] call XPT_fnc_loadItemCargo;
	};
	// Command SUV
	case (toLower "CUP_C_SUV_CIV"): {
		// Add the event handler
		_newVeh addMPEventHandler ["MPKilled", {
			// We're not making this same mistake again
			if (isServer) then {
				[] remoteExec ["SXP_fnc_respawnDisable", 0, false];
				missionNamespace setVariable ["SXP_var_respawnDisabled", true, true];
			};
		}];
		
		if !(isNil "_oldVeh") then {
			// If the vehicle is respawning, re-enable player respawns
			[] remoteExec ["SXP_fnc_respawnEnable", 0, false];
			_newVeh setVariable ["ACE_isRepairVehicle", true, true];
		};
		// Add item cargo
		[_newVeh, "suv"] call XPT_fnc_loadItemCargo;
	};
	case (toLower "C_Van_01_Box_F"): {
		_newVeh setObjectTextureGlobal [1,"media\textures\boxcar_canadapost.paa"];
	};
};