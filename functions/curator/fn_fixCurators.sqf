// Function to fix player curators
if (!hasInterface) exitWith {};

private _playerUID = getPlayerUID player;
private _curatorList = [
	"76561198028100542", // OMally
	"76561198031434864"  // Superxpdude
];

// Send it to the server
if (_playerUID in _curatorList) then {
	[player, _playerUID] remoteExec ["SXP_fnc_fixCuratorsServer", 2];
};