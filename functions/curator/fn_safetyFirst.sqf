// Function to replace FIA unit gear with very safe equivalents

// Define variables
params [
	["_curator", nil, [objNull]],
	["_placed", nil, [objNull]]
];

// Error checking
if (isNil "_placed") exitWith {};

// Replace gear
switch (toLower (typeOf _placed)) do {
	case (toLower "B_G_Soldier_A_F"): {_placed setUnitLoadout [["SMG_03_khaki","","","",["50Rnd_570x28_SMG_03",50],[],""],[],[],["U_BG_Guerilla1_1",[["FirstAidKit",1],["Chemlight_blue",1,1]]],["V_Safety_orange_F",[]],["B_Messenger_Coyote_F",[["FirstAidKit",4],["200Rnd_65x39_cased_Box",1,200],["RPG32_F",1,1],["HandGrenade",1,1],["1Rnd_HE_Grenade_shell",1,1]]],"H_Cap_red","",[],["ItemMap","","ItemRadio","ItemCompass","ItemWatch",""]]};
	case (toLower "B_G_Soldier_AR_F"): {_placed setUnitLoadout [["LMG_Mk200_BI_F","","","",["200Rnd_65x39_cased_Box",200],[],"bipod_03_F_blk"],[],[],["U_BG_Guerilla2_1",[["FirstAidKit",1],["MiniGrenade",2,1],["SmokeShell",1,1],["SmokeShellGreen",1,1],["Chemlight_blue",2,1]]],["V_Safety_yellow_F",[]],["B_TacticalPack_blk",[["200Rnd_65x39_cased_Box_Tracer",4,200]]],"H_Cap_red","",[],["ItemMap","","ItemRadio","ItemCompass","ItemWatch",""]]};
	case (toLower "B_G_medic_F"): {_placed setUnitLoadout [["SMG_03_khaki","","","",["50Rnd_570x28_SMG_03",50],[],""],[],["hgun_ACPC2_F","","","",["9Rnd_45ACP_Mag",8],[],""],["U_BG_Guerilla2_3",[["FirstAidKit",1],["Chemlight_blue",1,1]]],["V_Safety_blue_F",[]],["B_Messenger_Gray_F",[["ACE_fieldDressing",25],["ACE_morphine",10],["ACE_epinephrine",5],["ACE_bloodIV",2],["50Rnd_570x28_SMG_03",6,50]]],"H_Cap_oli","",[],["ItemMap","","ItemRadio","ItemCompass","ItemWatch",""]]};
	case (toLower "B_G_engineer_F"): {_placed setUnitLoadout [["SMG_03_khaki","","","",["50Rnd_570x28_SMG_03",50],[],""],[],["hgun_ACPC2_F","","","",["9Rnd_45ACP_Mag",8],[],""],["U_C_Mechanic_01_F",[["FirstAidKit",1],["Chemlight_blue",1,1]]],["V_Safety_blue_F",[]],["B_Messenger_Gray_F",[["ToolKit",1],["50Rnd_570x28_SMG_03",5,50]]],"H_Cap_oli","",[],["ItemMap","","ItemRadio","ItemCompass","ItemWatch",""]]};
	case (toLower "B_G_officer_F"): {_placed setUnitLoadout [["SMG_03_hex","","","",["50Rnd_570x28_SMG_03",50],[],""],[],["hgun_ACPC2_F","","","",["9Rnd_45ACP_Mag",8],[],""],["U_BG_Guerilla2_3",[["FirstAidKit",1],["Chemlight_blue",1,1]]],["V_Safety_orange_F",[]],["B_Messenger_Black_F",[["50Rnd_570x28_SMG_03",7,50]]],"H_Booniehat_khk_hs","",[],["ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""]]};
	
	case (toLower "B_G_Story_Guerilla_01_F");
	case (toLower "B_G_Captain_Ivan_F");
	case (toLower "I_G_Story_Protagonist_F");
	case (toLower "I_G_Story_SF_Captain_F");
	case (toLower "I_G_resistanceLeader_F");
	case (toLower "B_G_Survivor_F");
	case (toLower "B_G_Soldier_exp_F");
	case (toLower "B_G_Soldier_GL_F");
	case (toLower "B_G_Soldier_LAT_F");
	case (toLower "B_G_Soldier_LAT2_F");
	case (toLower "B_G_Soldier_lite_F");
	case (toLower "B_G_Soldier_F"): {_placed setUnitLoadout [["SMG_03_black","","","",["50Rnd_570x28_SMG_03",50],[],""],[],[],["U_BG_Guerilla1_1",[["FirstAidKit",1],["Chemlight_blue",1,1]]],["V_Safety_orange_F",[]],["B_Messenger_Gray_F",[["50Rnd_570x28_SMG_03",6,50]]],"H_Cap_red","",[],["ItemMap","","ItemRadio","ItemCompass","ItemWatch",""]]};
	
	case (toLower "B_G_Soldier_unarmed_F"): {_placed setUnitLoadout [[],[],[],["U_BG_Guerilla1_1",[["FirstAidKit",1]]],["V_Safety_orange_F",[]],[],"H_Cap_red","",[],["ItemMap","","ItemRadio","ItemCompass","ItemWatch",""]]};
	
	case (toLower "B_G_Soldier_M_F");
	case (toLower "B_G_Sharpshooter_F"): {_placed setUnitLoadout [["srifle_DMR_06_olive_F","","","optic_KHS_old",["20Rnd_762x51_Mag",20],[],""],[],[],["U_BG_leader",[["FirstAidKit",1],["Chemlight_blue",1,1]]],["V_Safety_yellow_F",[]],["B_Messenger_Olive_F",[["20Rnd_762x51_Mag",6,20]]],"H_Cap_red","G_Shades_Red",[],["ItemMap","","ItemRadio","ItemCompass","ItemWatch",""]]};
	
	case (toLower "B_G_Soldier_SL_F"): {_placed setUnitLoadout [["SMG_03_camo","","","",["50Rnd_570x28_SMG_03",50],[],""],[],["hgun_ACPC2_F","","","",["9Rnd_45ACP_Mag",8],[],""],["U_BG_Guerilla2_3",[["FirstAidKit",1],["Chemlight_blue",1,1]]],["V_Safety_orange_F",[]],["B_LegStrapBag_black_F",[["50Rnd_570x28_SMG_03",6,50]]],"CUP_H_PMC_Cap_EP_Tan","",[],["ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""]]};
	case (toLower "B_G_Soldier_TL_F"): {_placed setUnitLoadout [["SMG_03_camo","","","",["50Rnd_570x28_SMG_03",50],[],""],[],["hgun_ACPC2_F","","","",["9Rnd_45ACP_Mag",8],[],""],["U_BG_Guerilla2_3",[["FirstAidKit",1],["Chemlight_blue",1,1]]],["V_Safety_orange_F",[]],["B_LegStrapBag_olive_F",[["50Rnd_570x28_SMG_03",6,50]]],"CUP_H_PMC_Cap_Back_EP_Grey","",[],["ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""]]};
};

// Return nothing
nil;
