// Ensures that the safety first EH is applied to all curators

{
	// Add event handler to share placed units between curators
	_x addEventHandler ["CuratorObjectPlaced", {_this remoteExec ["SXP_fnc_safetyFirst", 2]}];
} forEach allCurators;