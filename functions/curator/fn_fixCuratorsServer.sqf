// Server portion of fn_fixCurators
if (!isServer) exitWith {};

params ["_player","_playerUID"];
private _curatorList = [
	"76561198028100542", // OMally
	"76561198031434864"  // Superxpdude
];

if (_playerUID in _curatorList) then {
	[_player] spawn compile format [
		"unassignCurator zeus_module_%1;
		sleep 3;
		(_this select 0) assignCurator zeus_module_%1;",
		_playerUID
	];
};