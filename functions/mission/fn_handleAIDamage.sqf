_this params ["_unit" ];

// Damage reduction from https://forums.bistudio.com/forums/topic/198136-reducing-player-damage/

// Exit if we are a player and not local
// Otherwise add EH for AI every where just in case their locality
// changes due to moving into a players group
// the EH will only fire where the AI is local
if (!local _unit) exitWith {};

_unit removeAllEventHandlers 'HandleDamage';


// Only damage from last applied handleDamage EH is taken into consideration by the engine
// Apply a new EH so as we can override the damage applied
_unit addEventHandler [ "HandleDamage", {
	params ["_unit", "_hitSelection", "_damage","_source","_projectile","_hitPartIndex", "_instigator", "_hitPoint"];

	// Damage multipliers.  The damage of each projectile will be multiplied by this number.
	private _damageMultiplier = 1;
	switch (typeOf _unit) do 
	{
		case "B_T_Soldier_F": {_damageMultiplier = 0.09;};
		case "B_T_UAV_03_dynamicLoadout_F": {_damageMultiplier = 0.15;};
		default {_damageMultiplier = 1;};
	};
	private _oldDamage = 0;
	if (_hitSelection isEqualTo "") then {_oldDamage = damage _unit} else {_oldDamage = _unit getHit _hitSelection};
	private _newDamage = _damage - _oldDamage max 0;
	private _incomingDamage = _newDamage;
	private _playerHealth = damage _unit;
	
	_newDamage = _newDamage * _damageMultiplier;

	_damage = _oldDamage + _newDamage;

	//systemChat format[ "pHealth: %1 selection: %2 oldTotal: %3 newTotal: %4 incomingDmg: %6 appliedDmg: %6", _playerHealth, _hitSelection, _oldDamage, _damage, _incomingDamage, _newDamage];

	_damage
	
}];
//systemChat format[ "Damage reduction applied to %1", _unit ];