// SXP_fnc_setupBillboard
// Sets up billboards with custom textures
// Only executes on the server, safe to use in init fields

if (!isServer) exitWith {};

_this params [
	["_board", objNull, [objNull]],
	["_type", nil, [""]]
];

if (isNull _board) exitWith {};
if (isNil "_type") exitWith {};

private _texture = switch (toLower _type) do {
	case (toLower "canadapost"): {"media\textures\bill_canadapost.paa"};
};

_board setObjectTextureGlobal [0,_texture];