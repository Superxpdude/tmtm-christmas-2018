// Function that allows using attachTo without losing the orientation of the object.
_object = _this select 0;
_target = _this select 1;
// Don't run unless the object is local
if (!local _object) exitWith {};

_object disableCollisionWith _target;
[_object, _target] call BIS_fnc_attachToRelative;