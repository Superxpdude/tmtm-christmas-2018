// Function for updating mission tasks when objectives are completed.
// Only execute on the server. Tasks should only be created server-side.
if (!isServer) exitWith {};

// Code for task updates goes into these fields. Can be any code required, including new task creation, task state updates, etc.
switch (toLower (_this select 0)) do {
	case "inventorysystem": {
		inventorySystem setVariable ["inspected", true, true];
		// Create the tasks if they have not been created yet
		if !(["locate_idap"] call BIS_fnc_taskExists) then {
			// Create the IDAP task
			[
				[independent, "zeus_unit"],
				["locate_idap","distribution_centre"], 
				"locate_idap", 
				objNull, 
				"CREATED", 
				0, 
				true, 
				"attack",
				true
			] call bis_fnc_taskCreate;
		};
		if !(["locate_ion"] call BIS_fnc_taskExists) then {
			// Create the ION task
			[
				[independent, "zeus_unit"],
				["locate_ion","distribution_centre"], 
				"locate_ion", 
				objNull, 
				"CREATED", 
				0, 
				true, 
				"attack",
				true
			] call bis_fnc_taskCreate;
		};
		if !(["locate_redstone"] call BIS_fnc_taskExists) then {
			// Create the Redstone task
			[
				[independent, "zeus_unit"],
				["locate_redstone","distribution_centre"], 
				"locate_redstone", 
				objNull, 
				"CREATED", 
				0, 
				true, 
				"attack",
				true
			] call bis_fnc_taskCreate;
		};
	};
	case "idapfound": {
		if (["locate_idap"] call BIS_fnc_taskExists) then {
			// If the task exists, update it.
			["locate_idap", "SUCCEEDED"] call BIS_fnc_taskSetState;
		} else {
			// If not, create it as a completed task
			[
				[independent, "zeus_unit"],
				["locate_idap","distribution_centre"], 
				"locate_idap", 
				objNull, 
				"SUCCEEDED", 
				0, 
				true, 
				"attack",
				true
			] call bis_fnc_taskCreate;
		};
		// Check if all distribution centre tasks have been completed
		["packagesfound"] call SXP_fnc_updateTask;
	};
	case "ionfound": {
		if (["locate_ion"] call BIS_fnc_taskExists) then {
			// If the task exists, update it.
			["locate_ion", "SUCCEEDED"] call BIS_fnc_taskSetState;
		} else {
			// If not, create it as a completed task
			[
				[independent, "zeus_unit"],
				["locate_ion","distribution_centre"], 
				"locate_ion", 
				objNull, 
				"SUCCEEDED", 
				0, 
				true, 
				"attack",
				true
			] call bis_fnc_taskCreate;
		};
		// Check if all distribution centre tasks have been completed
		["packagesfound"] call SXP_fnc_updateTask;
	};
	case "redstonefound": {
		if (["locate_redstone"] call BIS_fnc_taskExists) then {
			// If the task exists, update it.
			["locate_redstone", "SUCCEEDED"] call BIS_fnc_taskSetState;
		} else {
			// If not, create it as a completed task
			[
				[independent, "zeus_unit"],
				["locate_redstone","distribution_centre"], 
				"locate_redstone", 
				objNull, 
				"SUCCEEDED", 
				0, 
				true, 
				"attack",
				true
			] call bis_fnc_taskCreate;
		};
		// Check if all distribution centre tasks have been completed
		["packagesfound"] call SXP_fnc_updateTask;
	};
	case "packagesfound": {
		// Check if all packages have been found.
		if (("locate_idap" call BIS_fnc_taskCompleted) && ("locate_ion" call BIS_fnc_taskCompleted) && ("locate_redstone" call BIS_fnc_taskCompleted)) then {
			// If yes, mark the distribution centre task as completed.
			["distribution_centre", "SUCCEEDED"] call BIS_fnc_taskSetState;
		};
	};
	case "idaplocation": {
		idapPresent setVariable ["inspected", true, true];
		// Create the IDAP delivery task
		[
			[independent, "zeus_unit"],
			["deliver_idap","delivery"], 
			"deliver_idap", 
			idapLocation, 
			"CREATED", 
			0, 
			true, 
			"attack",
			true
		] call bis_fnc_taskCreate;
	};
	case "ionlocation": {
		ionPresent setVariable ["inspected", true, true];
		// Create the ION delivery task
		[
			[independent, "zeus_unit"],
			["deliver_ion","delivery"], 
			"deliver_ion", 
			ionLocation, 
			"CREATED", 
			0, 
			true, 
			"attack",
			true
		] call bis_fnc_taskCreate;
	};
	case "redstonelocation": {
		redstonePresent setVariable ["inspected", true, true];
		// Create the Redstone delivery task
		[
			[independent, "zeus_unit"],
			["deliver_redstone","delivery"], 
			"deliver_redstone", 
			redstoneLocation, 
			"CREATED", 
			0, 
			true, 
			"attack",
			true
		] call bis_fnc_taskCreate;
	};

	case "idappresent": {
		idapPresent setVariable ["delivered", true, true];
		private _pos = getPosATL presentHelipad;
		idapPresent setPosATL _pos;
		["deliver_idap", "SUCCEEDED"] call BIS_fnc_taskSetState;
		// Check if all delivery tasks have been completed
		["packagesdelivered"] call SXP_fnc_updateTask;
	};
	case "ionpresent": {
		ionPresent setVariable ["delivered", true, true];
		private _pos = getPosATL presentHelipad;
		_pos set [1, (_pos select 1) + 5];
		ionPresent setPosATL _pos;
		["deliver_ion", "SUCCEEDED"] call BIS_fnc_taskSetState;
		{
			_x hideObjectGlobal false;
		} forEach ((getMissionLayerEntities "IonDelivery") select 0);
		// Check if all delivery tasks have been completed
		[] spawn {
			private ["_script"];
			// Wait until the missile launch has finished before ending the mission
			_script = [ionMissile,ionMissileTarget] spawn SXP_fnc_missileLaunch;
			waitUntil {scriptDone _script};
			["packagesdelivered"] call SXP_fnc_updateTask;
		};
	};
	case "redstonepresent": {
		redstonePresent setVariable ["delivered", true, true];
		private _pos = getPosATL presentHelipad;
		_pos set [1, (_pos select 1) - 5];
		redstonePresent setPosATL _pos;
		["deliver_redstone", "SUCCEEDED"] call BIS_fnc_taskSetState;
		// Check if all delivery tasks have been completed
		["packagesdelivered"] call SXP_fnc_updateTask;
	};
	
	case "packagesdelivered": {
		// Check if all packages have been found.
		if (("deliver_idap" call BIS_fnc_taskCompleted) && ("deliver_ion" call BIS_fnc_taskCompleted) && ("deliver_redstone" call BIS_fnc_taskCompleted)) then {
			// If yes, mark the distribution centre task as completed.
			["delivery", "SUCCEEDED"] call BIS_fnc_taskSetState;
			
			// Introduce a delay on ending the mission
			[] spawn {
				sleep 5;
				// End the mission after all packages have been delivered
				["victory",true,true,true] remoteExec ["BIS_fnc_endMission", 0, true];
			};
		};
	};
};