// Handles launching a missile at a target
// Must be spawned, cannot be called

_this params [
	["_veh", objNull, [objNull]],
	["_target", nil, [[],objNull]]
];
private ["_script"];

// Error handling
if (isNull _veh) exitWith {};
if (isNil "_target") exitWith {};

if (_target isEqualType objNull) then {
	_target = getPos _target;
};

// Turn the engine on
[_veh, 1] remoteExec ["setFuel", _veh];
[_veh, true] remoteExec ["engineOn", _veh];

// Deploy the missile
_script = [_veh,1] spawn rhs_fnc_ss21_AI_prepare;
waitUntil {scriptDone _script};

// Wait 5 seconds
sleep 5;

// Launch the missile
_script = [_veh, _target] spawn rhs_fnc_ss21_AI_launch;
waitUntil {scriptDone _script};

// Turn the engine off
[_veh, 0] remoteExec ["setFuel", _veh];
[_veh, false] remoteExec ["engineOn", _veh];