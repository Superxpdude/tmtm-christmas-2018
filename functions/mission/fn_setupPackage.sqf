// SXP_fnc_setupPackage
// Handles setting up packages
// Must be executed on all machines. Call from the initi field of an object.

_this params [
	["_package", nil, [objNull]],
	["_type", nil, [""]]
];

if ((isNil "_package") OR (isNil "_type")) exitWith {};

_package allowDamage false;

switch (toLower _type) do {
	case (toLower "idap"): {
		[_package, ["media\labels\shippinglabel1.jpg",-1, -1], ""] call BIS_fnc_initInspectable;
		[
			_package,
			"Deliver package",
			"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_loadDevice_ca.paa",
			"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_loadDevice_ca.paa",
			"(!(_target getVariable ['delivered', false]) && ((player distance _target) < 10) && (_target in list delivery_idap))",
			"(!(_target getVariable ['delivered', false]) && ((player distance _target) < 10) && (_target in list delivery_idap))",
			nil,
			nil,
			{["idapPresent"] remoteExec ["SXP_fnc_updateTask", 2];},
			nil,
			[],
			5,
			2000, // Maximum priority
			false,
			false
		] call BIS_fnc_holdActionAdd;
	};
	case (toLower "ion"): {
		[_package, ["media\labels\shippinglabel2.jpg",-1, -1], ""] call BIS_fnc_initInspectable;
		[
			_package,
			"Deliver package",
			"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_loadDevice_ca.paa",
			"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_loadDevice_ca.paa",
			"(!(_target getVariable ['delivered', false]) && ((player distance _target) < 10) && (_target in list delivery_ion))",
			"(!(_target getVariable ['delivered', false]) && ((player distance _target) < 10) && (_target in list delivery_ion))",
			nil,
			nil,
			{["ionPresent"] remoteExec ["SXP_fnc_updateTask", 2];},
			nil,
			[],
			5,
			2000, // Maximum priority
			false,
			false
		] call BIS_fnc_holdActionAdd;
	};
	case (toLower "redstone"): {
		[_package, ["media\labels\shippinglabel3.jpg",-1, -1], ""] call BIS_fnc_initInspectable;
		[
			_package,
			"Deliver package",
			"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_loadDevice_ca.paa",
			"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_loadDevice_ca.paa",
			"(!(_target getVariable ['delivered', false]) && ((player distance _target) < 10) && (_target in list delivery_redstone))",
			"(!(_target getVariable ['delivered', false]) && ((player distance _target) < 10) && (_target in list delivery_redstone))",
			nil,
			nil,
			{["redstonePresent"] remoteExec ["SXP_fnc_updateTask", 2];},
			nil,
			[],
			5,
			2000, // Maximum priority
			false,
			false
		] call BIS_fnc_holdActionAdd;
	};
};