// initServer.sqf
// Executes only on the server at mission start
// No parameters are passed to this script

// Call the template initServer function
[] call XPT_fnc_initServer; // DO NOT CHANGE THIS LINE

// Call the script to handle initial task setup
[] execVM "scripts\tasks.sqf";



//////////////////////////////////////////////////////////
///// Add any mission specific code after this point /////
//////////////////////////////////////////////////////////

// Create a list of mission objects that should not be curator editable
XPT_blacklistedMissionObjects = [];

soundPath = [(str missionConfigFile), 0, -15] call BIS_fnc_trimString;
publicVariable "soundPath";

civie addEventHandler ["Hit", {
	params ["_unit", "_source", "_damage", "_instigator"];
	if(time >= civie getVariable ["timer", 0]) then {
	civie setVariable ["timer", time+3, true];
	playSound3D [soundPath + "media\sounds\missionfailed.ogg", civie, true, getPosASL civie, 5, 1, 50];
	};
}]; 

// Ensure that all FIA units are given safety gear
{
	[objNull, _x] call SXP_fnc_safetyFirst;
} forEach allMissionObjects "Man";

// Disable cargo inventories for shipping containers
{
	[_x, 0] call ace_cargo_fnc_setSpace;
} forEach allMissionObjects "Cargo_base_F";


addMissionEventHandler ["EntityKilled",{
	_destroyed = _this select 0;
	{
		if (_x isEqualType objNull) then {
			[_x,_destroyed] call ace_cargo_fnc_unloadItem;
		};
	} foreach (_destroyed getVariable ["ace_cargo_loaded",[]]);
}];
