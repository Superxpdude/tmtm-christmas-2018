// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point
[] execVM "scripts\briefing.sqf";

// Define the base sound path for custom sounds.
soundPath = [(str missionConfigFile), 0, -15] call BIS_fnc_trimString;

// Disable taking of robot clothes
player addEventHandler ["Take", {
    params ["_unitTaking", "_container", "_itemCN"];
    if (_itemCN == "H_RacingHelmet_4_F") then {
        _unitTaking unlinkItem _itemCN;
		_unitTaking removeItemFromUniform _itemCN;
		_unitTaking removeItemFromVest _itemCN;
		_unitTaking removeItemFromBackpack _itemCN;
		[_container, _itemCN] remoteExec ["addHeadgear", _container];
        systemChat "You can't take off the outer shell of a robot you dingus.";
    };
	if (_itemCN == "U_O_Protagonist_VR") then {
        _unitTaking unlinkItem _itemCN;
		[_container, _itemCN] remoteExec ["forceAddUniform", _container];
        systemChat "You can't take off the outer shell of a robot you dingus.";
    };
}];

// Inspectables
[inventorySystem, ["media\labels\shippingcomputer.jpg",-1, -1], ""] call BIS_fnc_initInspectable;

// Event handlers for inspecting the packages
missionNamespace setVariable ["presentInspectedEH", [missionNamespace, "objectInspected", 
	{
		_obj = _this select 0;
		if !(_obj getVariable ["inspected", false]) then {
			switch (toLower (vehicleVarName _obj)) do {
				case (toLower "idapPresent"): {
					["idaplocation"] remoteExec ["SXP_fnc_updateTask", 2];
				};
				case (toLower "ionPresent"): {
					["ionlocation"] remoteExec ["SXP_fnc_updateTask", 2];
				};
				case (toLower "redstonePresent"): {
					["redstonelocation"] remoteExec ["SXP_fnc_updateTask", 2];
				};
				case (toLower "inventorySystem"): {
					["inventorysystem"] remoteExec ["SXP_fnc_updateTask", 2];
				};
			};
		};
	}] call BIS_fnc_addScriptedEventHandler
];